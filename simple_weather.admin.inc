<?php

/**
 * Generate form for weather block
 *
 * @return array
 */
function simple_weather_admin_main_page() {

    $form['simple_weather_info'] = array(
        '#markup' => t('Below you have form do add new weather block. If cache is enabled sometimes it can be broken down. In this situation click here:').' <a href="'.url('simple_weather_clear_cache').'">'.t('clear cache').'</a>',
    );

    $form['simple_weather_api_key'] = array(
        '#title' => t('API key from http://openweathermap.org/'),
        '#type' => 'textfield',
        '#default_value' => variable_get('simple_weather_api_key'),
        '#description' => t('After registering API key is available in our profile.'),
        '#required' => true,
    );

    $form['simple_weather_city_name'] = array(
        '#title' => t('City name'),
        '#type' => 'textfield',
        '#default_value' => variable_get('simple_weather_city_name'),
        '#description' => t('These filed is used to get wether.'),
        '#required' => true,
    );

    $form['simple_weather_is_ajax'] = array(
        '#title' => t('Use ajax to render this block.'),
        '#type' => 'radios',
        '#options' => array(0=>t('No'), 1 => t('Yes')),
        '#default_value' => variable_get('simple_weather_is_ajax', 0),
        '#description' => t('Use ajax if full page cache is enabled.'),
        '#required' => true,
    );

    $form['simple_weather_cache_time'] = array(
        '#title' => t('Select cache time'),
        '#type' => 'select',
        '#options' => array(
            0=>t('No cache'),
            900 => t('15 minutes'),
            1800 => t('30 minutes'),
            3600 => t('1 hour'),
            7200 => t('2 hours'),
            14400 => t('4 hours')
        ),
        '#default_value' => variable_get('simple_weather_cache_time', 1800),
        '#description' => t('When ajax is enabled you can get new weather information more often than full page cache is set. In production environment we should always use cache.'),
        '#required' => true,
    );

    $form['simple_weather_show_wind_value'] = array(
        '#title' => t('Show wind info'),
        '#type' => 'radios',
        '#options' => array(0=>t('No'), 1 => t('Yes')),
        '#required' => true,
        '#default_value' => variable_get('simple_weather_show_wind_value', 0),
    );

    $form['simple_weather_show_pressure_value'] = array(
        '#title' => t('Show pressure info'),
        '#type' => 'radios',
        '#options' => array(0=>t('No'), 1 => t('Yes')),
        '#required' => true,
        '#default_value' => variable_get('simple_weather_show_pressure_value', 0),
    );

    $form['simple_weather_show_general_info'] = array(
        '#title' => t('Show general info text'),
        '#type' => 'radios',
        '#options' => array(0=>t('No'), 1 => t('Yes')),
        '#required' => true,
        '#default_value' => variable_get('simple_weather_show_general_info', 1),
		'#description' => t('Show one sentence information about weather'),
    );

    $form = system_settings_form($form);

	return $form;

}


/**
 *  Clear simple weather cache
 */
function simple_weather_clear_cache(){
    cache_clear_all('simple_weather', 'cache');
    drupal_set_message(t('Cache has been removed'));
    drupal_goto('admin/config/user-interface/simple_weather');
}