<div class="simple_weather_container">
	<ul>
		<li><img src="http://openweathermap.org/img/w/<?php echo $weather->icon ?>.png" title=""></li>
		<?php
		echo '<li>'.$weather->temperature_value.' °C</li>';

		if($block_config->show_general_info){
			echo '<li>'.$weather->weather_value.'</li>';
		}

		if($block_config->show_pressure_value){
			echo '<li>'.$weather->pressure_value.' '.$weather->pressure_unit.'</li>';
		}

		if($block_config->show_wind_value){
			echo '<li>'.$weather->wind_value.' MPS</li>';
		}
		?>
	</ul>
</div>