/*
 * If ajax is set, this function get content
 */
Drupal.behaviors.simple_weather = {
    attach: function (context, settings) {

        if (settings.simpleWeather !== undefined) {
            jQuery.ajax({
                url: Drupal.settings.basePath + Drupal.settings.pathPrefix + 'simple_weather_ajax',
                data: {simple_weather_block: settings.simpleWeather},
                type: 'POST',
                cache: false,
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    for (x in data) {
                        // put html to block
                        jQuery(x).replaceWith(data[x]);
                    }
                }
            });
        }
    }
};